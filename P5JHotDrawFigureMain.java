/* 
 * Problem 5. 아래 코드의 mouseClicked 메소드 내 if 문을 제거하여 코드를 업그레이드 하시오.
 * 실행 결과는 GUI출력
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

class CanvasEditor implements ActionListener, MouseListener {
    private JButton currentButton;

    public CanvasEditor(JButton initialButton) {
        this.currentButton = initialButton;
    }

    public void actionPerformed(ActionEvent ae) {
        currentButton = (JButton) ae.getSource();
    }

    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        JPanel canvas = (JPanel) e.getSource();
        if (currentButton.getText().equals("Ellipse"))
            canvas.getGraphics().drawOval(x - 30, y - 20, 60, 40);
        else if (currentButton.getText().equals("Rect"))
            canvas.getGraphics().drawRect(x - 30, y - 20, 60, 40);
        else //if( currentButton.getText().equals("Square") )
            canvas.getGraphics().drawRect(x - 25, y - 25, 50, 50);
    }

    public void mousePressed(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) { }
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
}

public class P5JHotDrawFigureMain extends JFrame {
    public P5JHotDrawFigureMain() {
        super("Drawing Application");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JComponent drawingCanvas = createDrawingCanvas();
        add(drawingCanvas, BorderLayout.CENTER);

        JToolBar toolbar = createToolbar(drawingCanvas);
        add(toolbar, BorderLayout.NORTH);
    }

    private JComponent createDrawingCanvas() {
        JComponent drawingCanvas = new JPanel();
        drawingCanvas.setPreferredSize(new Dimension(400, 300));
        drawingCanvas.setBackground(Color.white);
        drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
        return drawingCanvas;
    }

    private JToolBar createToolbar(JComponent canvas) {
        JToolBar toolbar = new JToolBar();
        JButton ellipseButton = new JButton("Ellipse");
        toolbar.add(ellipseButton);
        JButton squareButton = new JButton("Square");
        toolbar.add(squareButton);
        JButton rectButton = new JButton("Rect");
        toolbar.add(rectButton);

        CanvasEditor canvasEditor = new CanvasEditor(ellipseButton);
        ellipseButton.addActionListener(canvasEditor);
        squareButton.addActionListener(canvasEditor);
        rectButton.addActionListener(canvasEditor);
        canvas.addMouseListener(canvasEditor);

        return toolbar;
    }

	public static void main(String[] args) {
		P5JHotDrawFigureMain drawFrame = new P5JHotDrawFigureMain();
		drawFrame.pack();
		drawFrame.setVisible(true);
	}
}
