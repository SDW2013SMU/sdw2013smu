/* 
 * Problem 2. 아래 코드를 'Association'관계로 재작성하시오. 
 * 그림 'P2.png'을 참고하시오.
 * 아래와 같은 실행 결과를 출력하도록 main 메소드 코드의 빈 부분을 채우시오.
 * 
 * Problem 2. --------------------
 * *Hello*
 * (Hello)
 * -------------------------------
 */

interface Print {
	public abstract void printWeak();
	public abstract void printStrong();
}

class Banner {
	private String string;
	public Banner(String string) {
		this.string = string;
	}
	public void showWithParen() {
		System.out.println("("+string+")");
	}
	public void showWithAster() {
		System.out.println("*"+string+"*");
	}
}

class PrintBanner extends Banner implements Print {
	public PrintBanner(String string) {
		super(string);
	}
	public void printStrong() {
		showWithAster();
	}
	public void printWeak() {
		showWithParen();
	}
}

public class P2AdapterPatternMain {
	public static void main(String[] args) {
		System.out.println("Problem 2. -----------------");

		( ________ )		

		System.out.println("----------------------------");
	}
}

