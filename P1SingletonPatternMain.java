/*
 * Problem 1. 아래 코드를 Singleton pattern을 적용하여 재작성하시오. 
 * 아래와 같은 실행 결과를 출력하도록 main 메소드 코드의 빈 부분을 채우시오.
 * 
 * Problem 1. --------------------
 * Singleton log goes here
 * -------------------------------
 */

class SingletonPattern {
	public SingletonPattern() {}
	public void readEntireLog() {
		System.out.println("Singleton log goes here");
	}
}

public class P1SingletonPatternMain {
	public static void main(String[] args) {
        System.out.println("Problem 1. -----------------");
		SingletonPattern logger = ( ________ )
		logger.readEntireLog();
		System.out.println("----------------------------");
	}
}

