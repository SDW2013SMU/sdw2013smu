/*
 * Problem 4. 아래 코드의 빈 부분을 채우시오.  
 * 아래와 같은 실행 결과를 출력하도록 main 메소드 코드의 빈 부분을 채우시오.
 * 
 * Problem 4. --------------------
 * 나소공 상명대학교
 * 너설계 상명대학교
 * -------------------------------
 */

class ComplexClass implements Cloneable {
	private String universityName;
	private String department;
	private String grade;
	
	private String name;
	
	public ComplexClass(String uniName, String depart, String grade) {
		this.universityName = uniName;
		this.department = depart;
		this.grade = grade;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public String getName() {
		return name+" "+universityName;
	}

	@Override
	public Object clone() ( ________ ) {
		( ________ )
	}
}

public class P4PrototypePatternMain {
	public static void main(String[] args) {
		ComplexClass cc = new ComplexClass("상명대학교", "디지털미디어학부", "3학년");
		
		( ________ )

	}
}
