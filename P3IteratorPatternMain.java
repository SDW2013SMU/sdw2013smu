/* 
 * Problem 3. 아래 코드의 빈 부분을 채우시오.  
 * 아래와 같은 실행 결과를 출력하도록 main 메소드 코드의 빈 부분을 채우시오.
 * 
 * Problem 3. --------------------
 * Reading Statistics
 * R Cookbook
 * Django
 * -------------------------------
 */

import java.util.*;

interface Collection {
	public abstract Iterator iterator();
}

class Book {
	private String name = "";
	public Book(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}

class BookShelfCollection implements Collection {
	private int last = 0;
	private ArrayList<Book> books;
	
	public BookShelfCollection(int maxsize) {
		this.books = new ArrayList<Book>(maxsize);
	}
	public Book getBookAt(int index) {
		return books.get(index);
	}
	public void appendBook(Book book) {
		books.add(book);
		last++;
	}
	public int getLength() {
		return last;
	}
	public Iterator iterator() {
		return new BookShelfIterator(this);
	}
} 

interface Iterator {
	public abstract boolean hasNext();
	public abstract Object next();
}

class BookShelfIterator implements Iterator {
	private BookShelfCollection bookShelfCollection;
	private int index;

	public BookShelfIterator( ________ ) {
		( ________ )
	}

	public boolean hasNext() {
		( ________ )
	}
	public Object next() {
		( ________ )
	}
}

public class P3IteratorPatternMain {
	public static void main(String[] args) {
		BookShelfCollection bookShelfCollection = new BookShelfCollection(3);
		
		System.out.println("Problem 3. -----------------");
		
		( ________ )

		System.out.println("----------------------------");
	}
}
